﻿Shader "Study/1-primitive-shader" //shader definition
{
	//Sub shader program
	//possible to have multiple
	//for example to have different implementations for diffeent platforms or for different LODs
	SubShader
	{
		//Shader rendering pass
		//Needed for execution of shader program
		//Possible to have multiple
		//In case of multiple passes will be executed one after another
		Pass
		{
			CGPROGRAM //Defining shader programimg language (in current case its CG)
			#pragma vertex VertexProgram //Vertex program definition
			#pragma fragment FragmentProgram //Fragment program definition

			float4 VertexProgram() : SV_POSITION //Vertex program implementation
			{
				return 0;
			}

			float4 FragmentProgram(float4 pos : SV_POSITION) : SV_TARGET//Fragment program implementation
			{
				return 0;
			}

			ENDCG //end of CGPROGRAM
		}
	}
}
