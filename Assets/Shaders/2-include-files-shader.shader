﻿Shader "Study/2-include-files-shader" //shader definition
{
	//Sub shader program
	//possible to have multiple
	//for example to have different implementations for diffeent platforms or for different LODs
	SubShader
	{
		//Shader rendering pass
		//Needed for execution of shader program
		//Possible to have multiple
		//In case of multiple passes will be executed one after another
		Pass
		{
			CGPROGRAM //Defining shader programimg language (in current case its CG)
			#pragma vertex VertexProgram //Vertex program definition
			#pragma fragment FragmentProgram //Fragment program definition

			//include works similar to C/C++
			//so its possible to have duplicates
			//you can create your own include files in case if its needed
			//unity include files can be found in "unity-instalation-folder/CGIncludes/..."
			//you can go through all cg lib files using regular NotePad application
			#include "UnityCG.cginc"
			#include "UnityCG.cginc"

			void VertexProgram() //Vertex program implementation
			{

			}

			void FragmentProgram() //Fragment program implementation
			{

			}

			ENDCG //end of CGPROGRAM
		}
	}
}
