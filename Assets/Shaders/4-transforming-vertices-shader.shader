﻿//This shader shows how you can access vertex data in vertex program and pass it to fragment program 
Shader "Study/4-transformin-vertices-shader" //shader definition
{
	//Sub shader program
	//possible to have multiple
	//for example to have different implementations for diffeent platforms or for different LODs
	SubShader
	{
		//Shader rendering pass
		//Needed for execution of shader program
		//Possible to have multiple
		//In case of multiple passes will be executed one after another
		Pass
		{
			CGPROGRAM //Defining shader programimg language (in current case its CG)
			#pragma vertex VertexProgram //Vertex program definition
			#pragma fragment FragmentProgram //Fragment program definition

			//include works similar to C/C++
			//so its possible to have duplicates
			//you can create your own include files in case if its needed
			//unity include files can be found in "unity-instalation-folder/CGIncludes/..."
			//you can go through all cg lib files using regular NotePad application
			#include "UnityCG.cginc"

			//Vertex program implementation
			float4 VertexProgram
			//POSITION represents vertex local position
			(float4 position : POSITION)
			// : SV_POSITION - return (OUT) data representation where SV stands for "System value" and POSITION is returned vertex position
			: SV_POSITION 
			{
				return position; // passing local position of vertex to fragment program
			}

			//Fragment program implementation
			float4 FragmentProgram
			//(IN) iterated vertex position data generated in VertexProgram()
			//we need to provide SV_POSITION for compiler to know that variable "float4 position" represents vertex position from VertexProgram
			(float4 position : SV_POSITION) 
			// : SV_TARGET is a default render terget(render buffer/frame buffer) to which we are wrighting our pixels
			// you can provide your own buffer in case you dont want to write to camera frame buffer or for example in case of using MRT (multi render target) etc
			: SV_TARGET
			{
				return float4(0.0, 0.0, 0.0, 1.0);
			}

			ENDCG //end of CGPROGRAM
		}
	}
}
