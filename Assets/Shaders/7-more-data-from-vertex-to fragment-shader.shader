﻿//this shader will show how to pass more variables from vertex shader to fragment shader
Shader "Study/7-more-data-from-vertex-to-fragment-shader"
{
	//Block for properties that are passed from editor to shader
	Properties
	{
		//definition for Color property
		//without default value
		//_Color ("Color", Color)

		//definition for Color property
		//with default value
		_Color ("Color", Color) = (1, 1, 1, 1)
	}

	//Sub shader program
	//possible to have multiple
	//for example to have different implementations for diffeent platforms or for different LODs
	SubShader
	{
		//Shader rendering pass
		//Needed for execution of shader program
		//Possible to have multiple
		//In case of multiple passes will be executed one after another
		Pass
		{
			CGPROGRAM //Defining shader programimg language (in current case its CG)
			#pragma vertex VertexProgram //Vertex program definition
			#pragma fragment FragmentProgram //Fragment program definition

			//include works similar to C/C++
			//so its possible to have duplicates
			//you can create your own include files in case if its needed
			//unity include files can be found in "unity-instalation-folder/CGIncludes/..."
			//you can go through all cg lib files using regular NotePad application
			#include "UnityCG.cginc"

			//uniform is a keyword used in case you want to tell compiler that this value should be the same for all vertices and fragments
			//its not required to mark properties in CGPROGRAM cause this is done by compiler
			//but you should mark properties as uniform when you are working with native shaders like GLSL ...
			uniform 

			//you can use any suitable type for property if it will match incoming paramenters(for example half4)
			half4
			//name of variable should be exactly the same as in property field to be able to pass data from properties to shader variables
			_Color;

			//Vertex program implementation
			float4 VertexProgram
			//POSITION represents vertex local position
			(float4 position : POSITION,

			//out works similar to c# means that variable will be set in function
			out float3 localPosition :
			//TEXCOORD0 definition to pass data from verttex to fragment 
			//there is no special definition for custom variables this is the reason why everyone uses TEXCOORD
			//you can create any number of variables if system on which you are working supports it
			//TEXCOORD0, TEXCOORD1, TEXCOORD2, ... , TEXCOORD(N)
			TEXCOORD0)
			// : SV_POSITION - return (OUT) data representation where SV stands for "System value" and POSITION is returned vertex position
			: SV_POSITION 
			{
				//remember localPosition
				localPosition = position.xyz;
				//Will return vertex to screen position
				//return
				//Matrix by vector multiplication function
				//mul(
				//objects transformation to screen matrix
				//is otained from objects transform component(scale, position, rotation)
				//so it is basically calculated from objects transformation matrix multiplied by cameras(which is rendering this object at the moment) view projection matrix
				//UNITY_MATRIX_MVP,
				//vertex local position
				//position); 

				//same as above
				//wrapped into function
				//both work the same and you can use this method or simple multiplication
				return UnityObjectToClipPos(position);
			}

			//Fragment program implementation
			float4 FragmentProgram
			//(IN) iterated vertex position data generated in VertexProgram()
			//we need to provide SV_POSITION for compiler to know that variable "float4 position" represents vertex position from VertexProgram
			(float4 position : SV_POSITION,
			//localPosition from vertex program
			float3 localPosition : TEXCOORD0) 
			// : SV_TARGET is a default render terget(render buffer/frame buffer) to which we are wrighting our pixels
			// you can provide your own buffer in case you dont want to write to camera frame buffer or for example in case of using MRT (multi render target) etc
			: SV_TARGET
			{
				return float4(localPosition, 1.0) * _Color; //returning Color value from properties;
			}

			ENDCG //end of CGPROGRAM
		}
	}
}
