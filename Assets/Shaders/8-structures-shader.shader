﻿//this example will show how to work with structured data (structures)
Shader "Study/8-structures-shader"
{
	//Block for properties that are passed from editor to shader
	Properties
	{
		//definition for Color property
		//without default value
		//_Color ("Color", Color)

		//definition for Color property
		//with default value
		_Color ("Color", Color) = (1, 1, 1, 1)
	}

	//Sub shader program
	//possible to have multiple
	//for example to have different implementations for diffeent platforms or for different LODs
	SubShader
	{
		//Shader rendering pass
		//Needed for execution of shader program
		//Possible to have multiple
		//In case of multiple passes will be executed one after another
		Pass
		{
			CGPROGRAM //Defining shader programimg language (in current case its CG)
			#pragma vertex VertexProgram //Vertex program definition
			#pragma fragment FragmentProgram //Fragment program definition

			//include works similar to C/C++
			//so its possible to have duplicates
			//you can create your own include files in case if its needed
			//unity include files can be found in "unity-instalation-folder/CGIncludes/..."
			//you can go through all cg lib files using regular NotePad application
			#include "UnityCG.cginc"

			//structure for data going into vertex program
			struct VertexProgramIn
			{
				float4 position : POSITION;
			};

			//structure for data return from vertex program
			struct VertexProgramOut
			{
				float4 position : SV_POSITION;
				float3 localPosition : TEXCOORD0;
			};

			//uniform is a keyword used in case you want to tell compiler that this value should be the same for all vertices and fragments
			//its not required to mark properties in CGPROGRAM cause this is done by compiler
			//but you should mark properties as uniform when you are working with native shaders like GLSL ...
			uniform 

			//you can use any suitable type for property if it will match incoming paramenters(for example half4)
			half4
			//name of variable should be exactly the same as in property field to be able to pass data from properties to shader variables
			_Color;

			//Vertex program implementation
			VertexProgramOut VertexProgram
			//POSITION represents vertex local position
			(VertexProgramIn vertexProgramIn)
			{
				VertexProgramOut vertexProgramOut;

				//remember localPosition
				vertexProgramOut.localPosition = vertexProgramIn.position.xyz;

				//Will return vertex to screen position
				//return
				//Matrix by vector multiplication function
				//mul(
				//objects transformation to screen matrix
				//is otained from objects transform component(scale, position, rotation)
				//so it is basically calculated from objects transformation matrix multiplied by cameras(which is rendering this object at the moment) view projection matrix
				//UNITY_MATRIX_MVP,
				//vertex local position
				//position); 

				//same as above
				//wrapped into function
				//both work the same and you can use this method or simple multiplication
				vertexProgramOut.position = UnityObjectToClipPos(vertexProgramIn.position);
				return vertexProgramOut;
			}

			//Fragment program implementation
			float4 FragmentProgram
			//(IN) iterated vertex position data generated in VertexProgram()
			//we need to provide SV_POSITION for compiler to know that variable "float4 position" represents vertex position from VertexProgram
			(VertexProgramOut inFragmentProgram) 
			// : SV_TARGET is a default render terget(render buffer/frame buffer) to which we are wrighting our pixels
			// you can provide your own buffer in case you dont want to write to camera frame buffer or for example in case of using MRT (multi render target) etc
			: SV_TARGET
			{
				return float4(inFragmentProgram.localPosition, 1.0) * _Color; //returning Color value from properties;
			}

			ENDCG //end of CGPROGRAM
		}
	}
}
